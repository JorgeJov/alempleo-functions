import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

exports.createUserAccount = functions.database
    .ref('/applicants/{applicantId}')
    .onWrite((change, context) => {
        let email
        let pswd;
        let name;
        let alempleo;

        if (change.after) {
            email = change.after.val().email
            pswd = change.after.val().pswd
            name = change.after.val().name
            alempleo = change.after.val().alempleo
        }

        if (alempleo) {
            return admin.auth().createUser({
                uid: context.params.applicantId,
                email: email,
                password: pswd,
                displayName: name
            }).catch(error => {
                console.log("Error creating new user:", error);
            });
        } else {
            return true;
        }
    });

exports.createEnterpriseAccount = functions.database
    .ref('/user-enterprises/{enterpriseId}')
    .onWrite((change, context) => {
        let email
        let pswd;
        let name;
        let alempleo;

        if (change.after) {
            email = change.after.val().email
            pswd = change.after.val().pswd
            name = change.after.val().name
            alempleo = change.after.val().alempleo
        }

        if (alempleo) {
            return admin.auth().createUser({
                uid: context.params.enterpriseId,
                email: email,
                password: pswd,
                displayName: name
            }).catch(error => {
                console.log("Error creating new user:", error);
            });
        } else {
            return true;
        }
    });

exports.createAdminAccount = functions.database
    .ref('/admins/{adminId}')
    .onWrite((change, context) => {
        let email
        let pswd;
        let name;

        if (change.after) {
            email = change.after.val().email
            pswd = change.after.val().pswd
            name = change.after.val().name
        }

        return admin.auth().createUser({
            uid: context.params.adminId,
            email: email,
            password: pswd,
            displayName: name
        }).catch(error => {
            console.log("Error creating new user:", error);
        });

    });